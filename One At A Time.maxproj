{
	"name" : "One At A Time",
	"version" : 1,
	"creationdate" : 3576326128,
	"modificationdate" : 3580755122,
	"viewrect" : [ 25.0, 70.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"One At A Time.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}

		}
,
		"media" : 		{
			"SPLICE bass-electronics line 1 (harmonic over low G).wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"SPLICE bass-electronics line 2.wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"SPLICE bass-electronics line 3 (low E).wav" : 			{
				"kind" : "audiofile",
				"local" : 1
			}

		}
,
		"externals" : 		{

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0
}
